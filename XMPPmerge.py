#! /usr/bin/python3
import configparser
import base64
import datetime
import getpass
import logging
import os.path
import re
import ssl
import sys
from optparse import OptionParser

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QUrl
from PyQt5.QtGui import QDesktopServices, QIcon, QKeySequence, QPixmap
from PyQt5.QtWidgets import QAction, QApplication, QMainWindow, QShortcut
from PyQt5.QtWidgets import QWidget
from PyQt5.Qt import QVariant
from PyQt5.uic import loadUi
import sleekxmpp

import encrypt_decrypt
import keypromptDialog
import preferencesDialog


class App(QMainWindow):
    """ Widgets
        QAction: actionConnect
        QAction: actionDisconnect
        QLineEdit: lineEdit
        QMenuBar: menubar
        QMenu: menuRooms
        QMenu: menuOptions
        QMenu: menuConnection
        QPushButton: clearButton
        QPushButton: sendButton
        QShortcut: helpshortcut
        QStatusBar: statusbar
        QTextBrowser: textBrowser
    """
    xmpp_disco_items_trigger = pyqtSignal(str, str)
    xmpp_message_recieved_trigger = pyqtSignal(str)
    
    def __init__(self, path, title, config, logging):
        super(App, self).__init__()
        loadUi(path + '/ui/XMPPmerge.ui', self)
        self.setWindowTitle(title)
        self.textBrowser.setReadOnly(True)
        self.autojoin_executed = False
        self.config = config
        self.connected = False
        self.connected_false_pixmap = QPixmap(path + '/icons/status_red.png')
        self.connected_true_pixmap = QPixmap(path + '/icons/status_green.png')
        self.filtering = False
        self.filtering_criteria = ''
        self.helpshortcut = QShortcut(QKeySequence('Ctrl+H'), self)
        self.icon_counter = 1 # start at index one to skip grey icon/color
        self.icon_list = self._get_icons(path + '/icons/')
        self.messages = []
        self.path = path
        self.usage = '@|#|:<opt1>/<opt2>. @=chat, #=groupchat, :=command.'
        self._xmpp_get_muc_list_counter = datetime.datetime(
            1970, 1, 1, 0, 0, 0
            )
        
        if (self.config['connection']['plaintext_password'].lower() == 'true'):
            self._xmpp_password = self.config['connection']['password']
        else:
            # Need self._xmpp_password when entering password via
            # preferencesDialog after the application has been launched
            self._xmpp_password = False
        #Triggers
        self.helpshortcut.activated.connect(self.on_helpshortcut_activated)
        self.xmpp_disco_items_trigger.connect(self.xmpp_disco_items_recieved)
        self.textBrowser.anchorClicked.connect(self.on_anchor_clicked)
        self.xmpp_message_recieved_trigger.connect(self.xmpp_message_recieved)
        #Dialogs
        self.preferencesDialog = preferencesDialog.preferencesDialog(path,
            self.actionPreferences.text(), self.config, logging
            )
        self.preferencesDialog.buttonBox.accepted.connect(
            self.on_preferencesDialog_buttonBox_accept
            )
        self.keypromptDialog = keypromptDialog.keypromptDialog(path,
            "Password Encryption Key", self.config['connection']['password'],
            )
        self.keypromptDialog.buttonBox.accepted.connect(
            self.on_keypromptDialog_buttonBox_accept
        )

# Public Functions
    def closeEvent(self, event):
        """ Disconnect from the XMPP server on close """
        if self.connected is True:
            self.xmpp_disconnect()
        else:
            pass

    def filter_textBrowser(self, criteria):
        """ Filter the text in textBrowser based in user criteria """
        self.filtering = True
        self.filtering_criteria = criteria
        self.textBrowser.clear()
        for line in self.messages:
            if criteria in line:
                self.textBrowser.append(line)

    def xmpp_connect(self):
        """ Connect to the XMPP server
            xmpp_setup should be exectued first
        """
        if self._is_connect_ready() is True:
            if self.xmpp.connect():
                self.xmpp.process(block=False)
                return True
            else:
                logging.info('Unable to connect using '
                    + self.config['connection']['jid']
                    )
                return False
        else:
            return False

    def xmpp_disconnect(self):
        """ Disconnect the xmpp connection """
        self.xmpp.disconnect()
        self._set_connection_status(False)

    def xmpp_get_muc_list(self, conference):
        """ Get a list of the MUCs
            Triggers a disco_items event that calls self._xmpp_disco_items
        """
        if (datetime.datetime.now() - self._xmpp_get_muc_list_counter)\
                > datetime.timedelta(seconds=5):
            self._XMPPget_muc_list_counter = datetime.datetime.now()
            self.xmpp.plugin['xep_0030'].get_items(
                conference + '.' + self.xmpp.boundjid.host
                )
        else:
            logging.debug('xmpp_get_muc_list: Throttled iq query')

    def xmpp_join_muc(self, muc_name, conference):
        """ Joins the MUC muc_name in the Conference conference """
        logging.debug(
            'Joining MUC: ' + muc_name + '@' + conference + '.'
            + self.xmpp.boundjid.host
            )
        self.xmpp.plugin['xep_0045'].joinMUC(
            muc_name + '@' + conference + '.' + self.xmpp.boundjid.host,
            self.xmpp.boundjid.bare,
            wait=True
            )
        self._set_MUC_status(muc_name, True)

    def xmpp_leave_muc(self, muc_name, conference):
        """ Leaves the MUC muc_name in the Conference conference """
        logging.debug(
            'Leaving MUC: ' + muc_name + '@' + conference + '.'
            + self.xmpp.boundjid.host
            )
        self.xmpp.plugin['xep_0045'].leaveMUC(
            muc_name + '@' + conference + '.' + self.xmpp.boundjid.host,
            self.xmpp.boundjid.bare
            )
        self._set_MUC_status(muc_name, False)

    def xmpp_setup(self):
        """ Registers the needed XMPP plugins and adds the required event
            handlers
        """
        if self._get_required_xmpp_data() is True:
            self.xmpp = sleekxmpp.ClientXMPP(
                self.config['connection']['jid'],
                self._xmpp_password
                )
            self.xmpp.register_plugin('xep_0004') # Dataforms
            self.xmpp.register_plugin('xep_0030') # Service Discovery
            self.xmpp.register_plugin('xep_0045') # Multi-User Chat
            self.xmpp.register_plugin('xep_0060') # Publish / Subscribe
            self.xmpp.register_plugin('xep_0199') # XMPP Ping
            #self.xmpp.register_plugin('xep_0059') # Result Set Management
            self.xmpp.add_event_handler('disco_items', self._xmpp_disco_items)
            self.xmpp.add_event_handler('groupchat_message',
                self._xmpp_groupchat_message
                )
            self.xmpp.add_event_handler('message', self._xmpp_message)
            self.xmpp.add_event_handler('sent_presence', self._xmpp_sent_presence)
            self.xmpp.add_event_handler('session_start', self._xmpp_session_start)
            logging.debug('XMPP plugins and event handlers setup')
            return True
        else:
            return False

# Private Functions
    def _add_menu_item(self, name, muc_name):
        """ Adds an QAction to the menu menuRooms for each MUC that is passed
            with the name and muc_name arguments
        """
        if self.menuRooms.findChild(QAction, muc_name) is None:
            action = self.menuRooms.addAction(QIcon(self.icon_list['grey']), name)
            action.setObjectName(muc_name)
            if self.icon_counter > len(self.icon_list) - 1:
                self.icon_counter = 1 # start at index one to skip grey icon/color
            action.setProperty('color',
                QVariant(list(self.icon_list)[self.icon_counter])
                )
            action.setProperty('jid', QVariant(muc_name))
            action.setProperty('connected', False)
            action.triggered.connect(self.on_action_triggered)
            self.icon_counter += 1

    # Need to fix the auto join function. Doesn't update menu icons properly.
    # Not currently used. 
    #def _autojoin(self):
    #    for muc in self.config['chats']['autojoin_rooms'].split(','):
    #        muc_name = muc.strip()
    #        self.xmpp_join_muc(muc_name, opts.conference)

    def _decrypt_password(self, ciphertext, key):
        """ Decrypts ciphertext using key """
        decoded_cipher = base64.b64decode(ciphertext)
        return encrypt_decrypt.decrypt_text(decoded_cipher, key)

    def _encrypt_password(self, key, password):
        """ Encrypts password using key """
        pass

    def _get_icons(self, path):
        """ Scans path for icons and returns the results excluding items that
            should not be included
        """
        icons = {}
        icons['grey'] = QPixmap(path + '/grey.png') # set gray icon to index 0
        with os.scandir(path) as filelist:
            for file in filelist:
                if file.is_file() and not file.name.startswith('.') and not\
                        file.name.startswith('status_') and not\
                        file.name == 'grey.png':
                    icons[file.name.replace('.png', '')] = QPixmap(file.path)
        return icons

    def _get_required_xmpp_data(self):
        """
        """
        logging.debug('_get_required_xmpp_data() calling self._is_connect_ready()')
        ready_status = self._is_connect_ready()
        if ready_status is True:
            return True
        elif ready_status == 1:
            self.statusbar.showMessage("Please enter a jid")
            self.preferencesDialog.show()
            return False
        elif ready_status == 2:
            self.statusbar.showMessage("Please enter a password")
            self.preferencesDialog.show()
            return False
        elif ready_status == 3:
            self.statusbar.showMessage("Password is encrypted. Please enter decryption key")
            self.keypromptDialog.show()
            return False

    def _is_connect_ready(self):
        """ Determine if all parameters to establish XMPP connection are
            set
            Return values are:
              1 - jid not set in config
              2 - password not set in config
              3 - need password to be decrypted
        """
        if self.config['connection']['jid'] == ''\
            or self.config['connection']['jid'] is None:
            status = 1
        elif self.config['connection']['password'] == '':
            status = 2
        elif self.config['connection']['plaintext_password'].lower() == 'false'\
            and self._xmpp_password is False:
            status = 3
        else:
            status = True
        logging.debug('_is_connect_ready_called(). Returned status '
            + str(status))
        return status

    def _set_connection_status(self, connect):
        """ Sets the variables associated with the connections status """
        if connect is True:
            self.connected = True
            self.actionConnect.setDisabled(True)
            self.actionDisconnect.setDisabled(False)
            self.label.setPixmap(self.connected_true_pixmap)
        elif connect is False:
            self.connected = False
            self.actionConnect.setDisabled(False)
            self.actionDisconnect.setDisabled(True)
            self.label.setPixmap(self.connected_false_pixmap)
            self.menuRooms.clear()
        if self.statusbar.currentMessage() == 'Establishing Connection...':
            self.statusbar.clearMessage()
        logging.debug('Connected: ' + str(self.connected))

    def _set_MUC_status(self, muc_name, connected):
        """ Sets the status icon and properties of the QAction muc_name in the
            Menu menuRoom 
        """
        item = self.menuRooms.findChild(QAction, muc_name)
        if connected is True:
            item.setProperty('connected', True)
            item.setIcon(QIcon(self.icon_list[item.property('color')]))
        elif connected is False:
            item.setProperty('connected', False)
            item.setIcon(QIcon(self.icon_list['grey']))
        logging.debug(
            'menu item: ' + item.text() + '(' + item.property('jid')
            + ') Status: ' + str(item.property('connected'))
            )

    def _xmpp_disco_items(self, event):
        """ Triggered by the reception of a disco_items event that are
            returned from the execution of xmpp_get_muc_list
        """
        if event['type'] == 'result':
            for item in event['disco_items']:
                jid = item['jid'].split('@')[0]
                self.xmpp_disco_items_trigger.emit(item['name'], jid)
                logging.debug(
                    'Diso Item found jid: ' + jid + ' name: ' + item['name']
                    )

    def _xmpp_groupchat_message(self, msg):
        """ Triggered by the reception of a XMPP groupchat message """
        if msg['type'] in ('groupchat'):
            muc = str(msg['from']).split('@')[0]
            user = str(msg['from']).split('/')[1]
            item = self.menuRooms.findChild(QAction, muc)
            self.xmpp_message_recieved_trigger.emit('<span style="color:'
                + item.property('color') + '">'
                + datetime.datetime.now().strftime("%H:%M:%S") + ' ' + user
                + ': '+ msg['body'] + '</span>'
                )

    def _xmpp_message(self, msg):
        """ Triggered by the recpetion of an XMPP message """
        if msg['type'] in ('normal', 'chat'):
            user = str(msg['from']).split('@')[0]
            self.xmpp_message_recieved_trigger.emit(
                '<span style="color:black; background-color:grey">'
                + datetime.datetime.now().strftime("%H:%M:%S") + ' ' + user
                + ': '+ msg['body'] + '</span>'
                )

    def _xmpp_sent_presence(self, event):
        """ Triggered by the sending of an XMPP precence message """
        self._set_connection_status(True)
        self.xmpp_get_muc_list(self.config['chats']['conference'])
        #if self.autojoin_exectued is False:
        #    self._autojoin()
        #    self.autojoin_exectued = True

    def _xmpp_session_start(self, event):
        """ Triggered by the reception of an XMPP session_start event """
        self.xmpp.get_roster()
        self.xmpp.send_presence()

# Slots
    @pyqtSlot()
    def on_action_triggered(self):
        """ Triggered by the QAction menuRooms """
        logging.debug('on_action_triggered triggered')
        if self.connected is True:
            action = self.sender()
    
            if action.property('connected') is False:
                self.xmpp_join_muc(
                    action.property('jid'),
                    self.config['chats']['conference']
                    )
            else:
                self.xmpp_leave_muc(
                    action.property('jid'),
                    self.config['chats']['conference']
                    )
            if opts.loglevel == logging.DEBUG:
                self.textBrowser.append(
                    '<span style="color:' + action.property('color') + '">'
                    + action.text() + ' (' + action.property('jid')
                    + ') {status: ' + str(action.property('connected'))
                    + '}</span>'
                    )
        elif self.connected is False:
            pass
    
    @pyqtSlot()
    def on_actionConnect_triggered(self):
        """ Triggered by the QAction actionConnect """
        logging.debug('on_actionConnect_triggered triggered')
        if self.connected is True:
            return True
        if self.xmpp_setup() is True:
            self.statusbar.showMessage('Establishing Connection...')
            self.xmpp_connect()
            return True
        else:
            return False

    @pyqtSlot()
    def on_actionDisconnect_triggered(self):
        """ Triggered by the QAction actionDisconnect """
        logging.debug('on_actionDisconnect_triggered triggered')
        if self.connected is True:
            self.xmpp_disconnect()
        elif self.connected is False:
            pass

    @pyqtSlot()
    def on_actionPreferences_triggered(self):
        """ Triggered by the QAction actionPreferences """
        logging.debug('on_actionPreferences_triggered triggered')
        self.config.read(inifile)
        self.preferencesDialog.set_form_values(self.config)
        self.preferencesDialog.show()

    @pyqtSlot(QUrl)
    def on_anchor_clicked(self, url):
        """ Triggered when an Anchor is clicked in QTextBrowser textBrowser """
        logging.debug('on_anchor_clicked')
        QDesktopServices.openUrl(url)

    @pyqtSlot()
    def on_clearButton_clicked(self):
        """ Triggered by the QButton clearButton """
        logging.debug('on_clearButton_clicked triggered')
        self.lineEdit.clear()
        self.statusbar.clearMessage()
        if self.filtering is False:
            self.textBrowser.clear()
            self.messages = []
        elif self.filtering is True:
            self.filtering = False
            self.filtering_criteria = ''
            self.textBrowser.clear()
            for line in self.messages:
                self.textBrowser.append(line)

    @pyqtSlot()
    def on_helpshortcut_activated(self):
        """ Triggered when QShortcut helpshortcut  is activated """
        self.statusbar.showMessage('Usage:' + self.usage)

    @pyqtSlot()
    def on_menuRooms_aboutToShow(self):
        """ Triggered before showing the QMenu menuRooms. Updates the list of
            XMPP MUCs available
        """
        logging.debug('on_menuRooms_aboutToShow triggered')
        if self.connected is True:
            self.xmpp_get_muc_list(self.config['chats']['conference'])
        elif self.connected is False:
            pass

    @pyqtSlot()
    def on_keypromptDialog_buttonBox_accept(self):
        """ Sets the password after decrytion using the key provided by the
            user and triggers connection functions.
            Triggered when the password is set in the .ini, but it is
            encrypted. The keypromptDialog is displayed to get the key from
            the user.
        """
        logging.debug('on_keyprompt_buttonBox_accept triggered')
        self._xmpp_password = self._decrypt_password(
            self.config['connection']['password'],
            self.keypromptDialog.key0.text()
            )
        logging.debug('Password: ' + self._xmpp_password)
        self.on_actionConnect_triggered()
 
    @pyqtSlot()       
    def on_preferencesDialog_buttonBox_accept(self):
        """ Handles updating the .ini file with the preferences set by the
            user. Triggered when the preferencesDialog buttonBox Save is
            selected
        """
        logging.debug('on_preferencesDialog_buttonBox_accept triggered')
        self.config['connection']['jid'] = self.preferencesDialog.jid.text()
        if self.preferencesDialog.plaintext_password.isChecked() is True:
            self.config['connection']['plaintext_password'] = 'true'
        elif self.preferencesDialog.plaintext_password.isChecked() is False:
            self.config['connection']['plaintext_password'] = 'false'
        self.config['connection']['password'] =\
            self.preferencesDialog.password.text()
        self.config['chats']['autojoin_rooms'] =\
            self.preferencesDialog.autojoin_rooms.text()
        self.config['chats']['conference'] =\
            self.preferencesDialog.conference.text()
        if self.preferencesDialog.dynamic.isChecked() is True:
            self.config['chats']['dynamic'] = 'true'
        elif self.preferencesDialog.dynamic.isChecked() is False:
            self.config['chats']['dynamic'] = 'false'
        with open(inifile, 'w') as file:
            self.config.write(file)
        self.statusbar.clearMessage()
        logging.debug('{jid}' +
            self.config['connection']['jid'] + ':'
            '{password}' +  self.config['connection']['password'] + ':'
            '{plaintext_password}' + self.config['connection']['plaintext_password'] + ':'
            '{autojoin_rooms}' + self.config['chats']['autojoin_rooms'] + ':'
            '{conference}' + self.config['chats']['conference'] + ':'
            '{dynamic}' + self.config['chats']['dynamic']
            )

    @pyqtSlot()
    def on_sendButton_clicked(self):
        """ Sends a message to the user defined in QLineEdit lineEdit when the
            sendButton is clicked by the user
        """
        logging.debug('on_sendButton_clicked triggered')
        matches = re.match(
            "(?P<cmd_type>\@|\#|\:)(?P<option1>[a-zA-Z0-9_\-\@\.]+)/(?P<option2>.+)",
            self.lineEdit.text()
            )
        if matches:
            m = matches.groupdict()
            if m['cmd_type'] == '#':
                cmd_type = 'groupchat'
            elif m['cmd_type'] == '@':
                cmd_type = 'chat'
            elif m['cmd_type'] == ':' and m['option1'] == 'filter':
                self.filter_textBrowser(m['option2'])
                return True
            
            if '@' not in m['option1']:
                if cmd_type == 'chat':
                    user = m['option1'] + '@' + self.xmpp.boundjid.host
                elif cmd_type == 'groupchat':
                    user = m['option1'] + '@' + self.config['chats']['conference']\
                        + '.' + self.xmpp.boundjid.host
            else:
                user = m['option1']
        
            logging.debug('user:' + user + " message:" + m['option2']
                + ' message type:' + cmd_type)    
            self.xmpp.send_message(mto=user, mbody=m['option2'], mtype=cmd_type)
            self.lineEdit.clear()
        else:
            self.statusbar.showMessage(
                'Message format is incorrect. Usage: ' + self.usage,
                5000
                )

    @pyqtSlot(str, str)
    def xmpp_disco_items_recieved(self, name, muc_name):
        """ Triggered whenever a XMPP disco_items event is recieved. Call
            _add_menu_item to add the MUC to the QMenu menuRooms
        """
        logging.debug('disco_items_recieved triggered')
        self._add_menu_item(name, muc_name)

    @pyqtSlot(str)
    def xmpp_message_recieved(self, message):
        """ Triggered whenever a XMPP message is received """
        logging.debug(message)
        self.messages.append(message)
        if self.filtering is True:
            if self.filtering_criteria in message:
                self.textBrowser.append(message)
        else:
            self.textBrowser.append(message)

#-------------------functions--------------------
def loadconfig(inifile, write_blank=False):
    config = configparser.ConfigParser()
    if write_blank is True or os.path.isfile(inifile) is False:
        print('Generating default values for ' + inifile)
        config['connection'] = {
            'jid' : '',
            'password' : '',
            'plaintext_password' : 'true'
            }
        config['chats'] = {
        'autojoin_rooms' : '',
        'conference' : 'conference',
        'dynamic' : 'true'
        }
        with open(inifile, 'w') as file:
            config.write(file)
    else:
        config.read(inifile)
    return config
#-------------------functions--------------------


if __name__ == '__main__':
    inifile =  __file__.replace('.py', '.ini')
    config = loadconfig(inifile)
    path = os.path.dirname(os.path.realpath(__file__))
    parser = OptionParser()
    parser.add_option('-d', '--debug',
        action='store_const',
        dest='loglevel',
        const=logging.DEBUG,
        default=logging.INFO,
        help='Set logging level to debug')
    opts, args = parser.parse_args()
    logging.basicConfig(
        level=opts.loglevel,
        format='%(levelname)-8s %(message)s'
        )

    app = QApplication(sys.argv)
    exe = App(path, "XMPP Merge", config, logging)
    exe.show()
    # Prompt for required XMPP data if not set in .ini
    if config['connection']['jid'] == '' or\
            config['connection']['password'] == '':
        logging.debug('Displaying Preferences Dialog')
        exe.preferencesDialog.show()

    sys.exit(app.exec_())