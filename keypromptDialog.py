#!/usr/bin/python3
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

class keypromptDialog(QDialog):
    # Widgets
    # QDialogButtonBox: buttonBox
    # QLineEdit: key0
    def __init__(self, path, title, password):
        super(keypromptDialog, self).__init__()
        loadUi(path + '/ui/keypromptDialog.ui', self)
        self.setWindowTitle(title)
        self.setModal(True)