#!/usr/bin/python3
import base64

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

import cryptDialog
import encrypt_decrypt


class preferencesDialog(QDialog):
    # Widgets
    # QDialogButtonBox: buttonBox
    # QLineEdit: jid
    # QLineEdit: password
    # QCheckBox: plaintext_password
    # QLineEdit: autojoin_rooms
    # QLineEdit: conference
    # QCheckBox: dynamic
    def __init__(self, path, title, config, logging):
        super(preferencesDialog, self).__init__()
        loadUi(path + '/ui/preferencesDialog.ui', self)
        self.setWindowTitle(title)
        self.setModal(True)
        self.trigger_keyprompt = False
        self.cryptDialog = cryptDialog.cryptDialog(path, "Encrypt Password", logging)
        self.set_form_values(config)
        self.trigger_keyprompt = True

#Public Functions
    def set_form_values(self, config):
        #[connection]
        self.jid.setText(config['connection']['jid'])
        self.password.setText(config['connection']['password'])

        if config['connection']['plaintext_password'].lower() == 'false':
            self.plaintext_password.setChecked(False)
        else:
            self.plaintext_password.setChecked(True)

        #[chats]
        self.autojoin_rooms.setText(config['chats']['autojoin_rooms'])
        self.conference.setText(config['chats']['conference'])

        if config['chats']['dynamic'].lower() == 'true':
            self.dynamic.setChecked(True)
        else:
            self.dynamic.setChecked(False)

# Private Functions
    def _encrypt_password(self):
        """ Encrypt password using key from cryptDialog """
        ciphertext = encrypt_decrypt.encrypt_text(
            self.cryptDialog.password0.text().strip(),
            self.cryptDialog.key0.text().strip())
        self.password.setText(base64.b64encode(ciphertext).decode('utf-8'))
        self.plaintext_password.setChecked(False)

# Slots
    @pyqtSlot(int)
    def on_plaintext_password_stateChanged(self, state):
        # state == 0 is unchecked, state == 1 is partially checked, state == 2 is unchecked
        if state == 0:
            self.cryptDialog.buttonBox.accepted.connect(self._encrypt_password)
            if self.password.text() != '':
                self.cryptDialog.password0.setText(self.password.text())
                self.cryptDialog.password1.setText(self.password.text())
            if self.trigger_keyprompt is True:
                self.cryptDialog.show()
        elif state == 2:
            self.password.setText('')
