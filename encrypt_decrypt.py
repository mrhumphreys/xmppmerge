#!/usr/bin/python3
import hashlib

from Crypto import Random
from Crypto.Cipher import AES

def decrypt_text(ciphertext, password):
    iv = ciphertext[:16]
    ciphertext = ciphertext[16:]
    key = make_key(password, 32)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    padded_plaintext = cipher.decrypt(ciphertext)
    padded_plaintext = padded_plaintext.decode('utf-8')
    plaintext = unpad_text(padded_plaintext)
    return plaintext

def encrypt_text(plaintext, password):
    iv = Random.get_random_bytes(16)
    key =  make_key(password, 32)
    padded_plaintext = pad_text(plaintext)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    ciphertext = cipher.encrypt(padded_plaintext)
    ciphertext = iv + ciphertext
    return ciphertext

def make_key(password, iterations):
    key = bytes(password, 'utf-8')
    for i in range(iterations):
        key = hashlib.sha256(key).digest()
    return key

def pad_text(value):
    extra = len(value) % 16
    padlength = 16 - extra
    padding = chr(padlength) * padlength
    return value + padding

def unpad_text(padded_value):
    padlength = ord(padded_value[-1])
    return padded_value[:-padlength]