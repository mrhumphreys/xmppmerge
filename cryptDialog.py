#!/usr/bin/python3
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi


class cryptDialog(QDialog):
    # Widgets
    # QDialogButtonBox: buttonBox
    # QLineEdit: password0
    # QLineEdit: password1
    # QLineEdit: key0
    # QLineEdit: key1
    def __init__(self, path, title, logging):
        super(cryptDialog, self).__init__()
        loadUi(path + '/ui/cryptDialog.ui', self)
        self.setWindowTitle(title)
        self.password1.textChanged.connect(self.on_password1_textChanged)
        self.key1.textChanged.connect(self.on_key1_textChanged)
        self.logging = logging

# Private Functions
    def _validate_objects(self, object0, object1):
        if object0.text() == object1.text():
            object1.setStyleSheet('background-color : white')
            self.logging.debug(object1.text() + ': Changing Sytle Sheet to White')
        else:
            object1.setStyleSheet('background-color : red')
            self.logging.debug(object1.text() + ': Changing Sytle Sheet to red')

#Slots
    @pyqtSlot()
    def on_password1_textChanged(self):
        self._validate_objects(self.password0, self.password1)
        self.logging.debug("on_password1_textChanged fired!")

    @pyqtSlot()
    def on_key1_textChanged(self):
        self._validate_objects(self.key0, self.key1)
        self.logging.debug("on_key1_textChanged fired!")